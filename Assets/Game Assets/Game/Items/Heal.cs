﻿using UnityEngine;

namespace Game_Assets.Game.Items
{
    /**
     * BANDAGE, MEDKIT, SYRINGE
     */
    public class Heal : Item, IConsumable, IStackable
    {
        private int maxStackAmount;

        public void Consume()
        {
        }

        public void Stack()
        {
        }
    }
}