﻿using System;
using UnityEngine;

namespace Game_Assets.Game.Items
{
    /**
     * AK, M4A1, AWP, DEAGLE
     */
    public class Weapon : Item, IEquippable
    {
        [SerializeField] WeaponData data;

        public void Equip()
        {
        }
    }

    [Serializable]
    public struct WeaponData
    {
        public Sprite sprite;
        public WeaponType weaponType;
        public AmmoType ammoType;
        public int damage;
        public int fireRate;
        public int magazine;
        public int ammoCapacity;
    }

    public enum WeaponType
    {
        AR,
        SMG,
        SNIPER,
        PISTOL,
        MELEE
    }
}