﻿namespace Game_Assets.Game.Items
{
    /**
     * WEAPON, ARMOR
     */
    public interface IEquippable
    {
        void Equip();
    }
}