﻿namespace Game_Assets.Game.Items
{
    //TACTICAL
    interface IThrowable
    {
        void Throw();
    } 
}