﻿using UnityEngine;

namespace Game_Assets.Game.Items
{
    public class Item : MonoBehaviour
    {
        private int id = -1;
        private int amount = 1;
        private int maxStackAmount = 1;

        public int ID
        {
            get => id;
            set => id = value;
        }

        public int Amount
        {
            get => amount;
            set => amount = value;
        }

        public int MaxStackAmount
        {
            get => maxStackAmount;
            set => maxStackAmount = value;
        }
    }
}