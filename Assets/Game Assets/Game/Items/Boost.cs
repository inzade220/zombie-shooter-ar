﻿using UnityEngine;

namespace Game_Assets.Game.Items
{
    /**
     * ENERGY_DRINK, PAINKILLER
     */
    public class Boost : Item, IConsumable, IStackable
    {
        private int maxStackAmount;

        public void Consume()
        {
        }

        public void Stack()
        {
        }
    }
}