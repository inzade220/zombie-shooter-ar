﻿namespace Game_Assets.Game.Items
{
    public interface IStackable
    {
        void Stack();
    }
}