﻿namespace Game_Assets.Game.Items
{
    /**
     * HEAL, BOOST
     */
    interface IConsumable
    {
        void Consume();
    }
}