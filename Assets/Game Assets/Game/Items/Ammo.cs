﻿namespace Game_Assets.Game.Items
{
    public class Ammo : Item, IStackable, IConsumable
    {
        public void Stack()
        {
        }

        public void Consume()
        {
            
        }
    }

    public enum AmmoType
    {
        NONE,
        AR,
        SMG,
        SNIPER,
        PISTOL
    }
}