﻿using System.Collections;
using Devkit.Base.Component;
using Firebase;
using Firebase.Auth;
using Firebase.Database;
using UnityEngine;

namespace ZombieShooter.Component
{
    public class FirebaseComponent : MonoBehaviour, IComponent
    {
        [Space(10f)] private FirebaseApp firebaseApp;
        public static FirebaseUser firebaseUser;
        private FirebaseAuth firebaseAuth;
        private DependencyStatus dependencyStatus;
        private bool autoLogin = false;


        public delegate void FirebaseComponentDelegate(bool success);

        public event FirebaseComponentDelegate OnAutoLoginCheck;
        public event FirebaseComponentDelegate OnLogin;
        public event FirebaseComponentDelegate OnRegister;

        public void Initialize(ComponentContainer componentContainer)
        {
            StartFirebaseConnection();
        }

        private void StartFirebaseConnection()
        {
            FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
            {
                dependencyStatus = task.Result;
                if (dependencyStatus == DependencyStatus.Available)
                {
                    //If they are available Initialize Firebase
                    InitializeFirebase();
                }
                else
                {
                    Debug.LogError("Could not resolve all Firebase dependencies: " + dependencyStatus);
                }
            });
        }

        private void InitializeFirebase()
        {
            //Set the authentication instance object
            firebaseAuth = FirebaseAuth.DefaultInstance;
            firebaseAuth.StateChanged += AuthStateChanged;
            AuthStateChanged(this, null);
        }

        public void CheckAutoLogin()
        {
            if (firebaseUser != null)
            {
                firebaseUser.ReloadAsync().ContinueWith(task =>
                {
                    if (task.IsCompleted)
                    {
                        Debug.Log("Auto Login Completed");
                        OnAutoLoginCheck(true);
                    }
                    else if (task.IsCanceled)
                    {
                        Debug.Log("Auto Login Is Canceled!");
                        OnAutoLoginCheck(false);
                    }
                    else
                    {
                        Debug.Log("Auto Login Is Faulted!");
                        OnAutoLoginCheck(false);
                    }
                });
            }
            else
            {
                Debug.Log("User Not Found!");
                OnAutoLoginCheck(false);
            }
        }

        private void AuthStateChanged(object sender, System.EventArgs eventArgs)
        {
            if (firebaseAuth.CurrentUser != firebaseUser)
            {
                bool signedIn = firebaseUser != firebaseAuth.CurrentUser && firebaseAuth.CurrentUser != null;

                if (!signedIn && firebaseUser != null)
                {
                    Debug.Log("Signed Out");
                }

                firebaseUser = firebaseAuth.CurrentUser;

                if (signedIn)
                {
                    OnLogin(true);
                }
            }
        }

        public void Login(string _email, string _password)
        {
            StartCoroutine(CoLogin(_email, _password));
        }

        public void Register(string _email, string _password)
        {
            StartCoroutine(CoRegister(_email, _password));
        }

        private IEnumerator CoLogin(string _email, string _password)
        {
            Debug.Log("LOGIN REQUEST SENT");
            //Call the Firebase auth signin function passing the email and password
            var LoginTask = firebaseAuth.SignInWithEmailAndPasswordAsync(_email, _password);
            //Wait until the task completes
            yield return new WaitUntil(predicate: () => LoginTask.IsCompleted);

            if (LoginTask.Exception != null)
            {
                //If there are errors handle them
                Debug.LogWarning(message: $"Failed to register task with {LoginTask.Exception}");
                FirebaseException firebaseEx = LoginTask.Exception.GetBaseException() as FirebaseException;
                AuthError errorCode = (AuthError) firebaseEx.ErrorCode;

                string message = "Login Failed!";
                switch (errorCode)
                {
                    case AuthError.MissingEmail:
                        message = "Missing Email";
                        break;
                    case AuthError.MissingPassword:
                        message = "Missing Password";
                        break;
                    case AuthError.WrongPassword:
                        message = "Wrong Password";
                        break;
                    case AuthError.InvalidEmail:
                        message = "Invalid Email";
                        break;
                    case AuthError.UserNotFound:
                        message = "Account does not exist";
                        break;
                }

                Debug.Log(message);
                OnLogin(false);
            }
            else
            {
                //User is now logged in
                //Now get the result
                firebaseUser = LoginTask.Result;
                Debug.LogFormat("User signed in successfully: {0} ({1})", firebaseUser.DisplayName, firebaseUser.Email);
                OnLogin(true);
            }
        }

        private IEnumerator CoRegister(string _email, string _password)
        {
            //Call the Firebase auth signin function passing the email and password
            var RegisterTask = firebaseAuth.CreateUserWithEmailAndPasswordAsync(_email, _password);
            //Wait until the task completes
            yield return new WaitUntil(predicate: () => RegisterTask.IsCompleted);

            if (RegisterTask.Exception != null)
            {
                //If there are errors handle them
                Debug.LogWarning(message: $"Failed to register task with {RegisterTask.Exception}");
                FirebaseException firebaseEx = RegisterTask.Exception.GetBaseException() as FirebaseException;
                AuthError errorCode = (AuthError) firebaseEx.ErrorCode;

                string message = "Register Failed!";
                switch (errorCode)
                {
                    case AuthError.MissingEmail:
                        message = "Missing Email";
                        break;
                    case AuthError.MissingPassword:
                        message = "Missing Password";
                        break;
                    case AuthError.WeakPassword:
                        message = "Weak Password";
                        break;
                    case AuthError.EmailAlreadyInUse:
                        message = "Email Already In Use";
                        break;
                }

                Debug.Log(message);
                OnRegister(false);
            }
            else
            {
                //User has now been created
                //Now get the result
                firebaseUser = RegisterTask.Result;

                if (firebaseUser != null)
                {
                    //Create a user profile and set the username
                    UserProfile profile = new UserProfile {DisplayName = "Player"};

                    //Call the Firebase auth update user profile function passing the profile with the username
                    var ProfileTask = firebaseUser.UpdateUserProfileAsync(profile);
                    //Wait until the task completes
                    yield return new WaitUntil(predicate: () => ProfileTask.IsCompleted);

                    if (ProfileTask.Exception != null)
                    {
                        //If there are errors handle them
                        Debug.LogWarning(message: $"Failed to register task with {ProfileTask.Exception}");
                        FirebaseException firebaseEx = ProfileTask.Exception.GetBaseException() as FirebaseException;
                        AuthError errorCode = (AuthError) firebaseEx.ErrorCode;
                        Debug.Log("Username Set Failed!");
                    }
                }

                OnRegister(true);
                Login(_email, _password);
            }
        }
    }
}