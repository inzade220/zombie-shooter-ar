﻿using System;
using System.Collections;
using System.Threading.Tasks;
using Devkit.Base.Component;
using Devkit.Base.Object;
using Firebase;
using Firebase.Auth;
using Firebase.Database;
using Firebase.Extensions;
using Game_Assets.Base;
using JetBrains.Annotations;
using UniRx;
using UniRx.Triggers;
using UniRx.Operators;
using UnityEngine;

namespace ZombieShooter.Component
{
    public class DatabaseComponent : MonoBehaviour, IComponent
    {
        #region Variables

        private FirebaseComponent firebaseComponent;
        private LoadComponent loadComponent;
        private SaveComponent saveComponent;

        private FirebaseDatabase firebaseDatabase;

        #endregion

        public void Initialize(ComponentContainer componentContainer)
        {
            firebaseComponent = componentContainer.GetComponent("FirebaseComponent") as FirebaseComponent;
            loadComponent = componentContainer.GetComponent("LoadComponent") as LoadComponent;
            saveComponent = componentContainer.GetComponent("SaveComponent") as SaveComponent;

            firebaseDatabase = FirebaseDatabase.DefaultInstance;
        }

        public void Load(ICanLoadData sender, string path = "")
        {
            DataSnapshot dataSnapshot = null;
            DatabaseReference databaseReference;

            if (path == "")
            {
                databaseReference = firebaseDatabase.RootReference.Child("users")
                    .Child(FirebaseComponent.firebaseUser.UserId);
            }
            else
                databaseReference = firebaseDatabase.RootReference.Child("users")
                    .Child(FirebaseComponent.firebaseUser.UserId).Child(path);

            databaseReference.GetValueAsync().ContinueWith(task =>
            {
                if (task.IsFaulted)
                    sender.OnLoadProcessFaulted();
                if (task.IsCanceled)
                    sender.OnLoadProcessCanceled();
                if (!task.IsCompleted) return;
                dataSnapshot = task.Result;
                if (dataSnapshot.Exists)
                    sender.OnLoadProcessCompleted(dataSnapshot);
                else
                    sender.OnLoadProcessDataNotExists();
            });
        }

        public void Save<T>(ICanSaveData sender, T data, string path = "")
        {
            string json = JsonUtility.ToJson(data);
            DatabaseReference databaseReference;

            if (path == "")
            {
                databaseReference = firebaseDatabase.RootReference.Child("users")
                    .Child(FirebaseComponent.firebaseUser.UserId);
            }
            else
                databaseReference = firebaseDatabase.RootReference.Child("users")
                    .Child(FirebaseComponent.firebaseUser.UserId).Child(path);

            databaseReference.SetRawJsonValueAsync(json).ContinueWith(task =>
            {
                if (task.IsFaulted)
                    sender.OnSaveProcessFaulted();
                if (task.IsCanceled)
                    sender.OnSaveProcessCanceled();
                if (task.IsCompleted)
                    sender.OnSaveProcessCompleted();
            });
        }
    }
}