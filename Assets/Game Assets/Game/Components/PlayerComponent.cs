﻿using System;
using Devkit.Base.Component;
using UnityEngine;

namespace ZombieShooter.Component
{
    public class PlayerComponent : MonoBehaviour, IComponent
    {
        [SerializeField] private GameObject onMapPlayer;
        [SerializeField] private GameObject onGamePlayer;

        private СhestComponent chestComponent;
        private CurrencyComponent currencyComponent;
        private InventoryComponent inventoryComponent;

        public void Initialize(ComponentContainer componentContainer)
        {
            chestComponent = componentContainer.GetComponent("ChestComponent") as СhestComponent;
            currencyComponent = componentContainer.GetComponent("CurrencyComponent") as CurrencyComponent;
            inventoryComponent = componentContainer.GetComponent("InventoryComponent") as InventoryComponent;
        }

        private void OnTriggerEnter(Collider other)
        {
            Debug.Log("Triggered");
            if (other.gameObject.CompareTag("Chest"))
            {
                chestComponent.OpenChest();
                currencyComponent.EarnMoney(chestComponent.GetDropGold());
                other.gameObject.Destroy();
            }
        }

        public void SwitchPlayerPrefab(PlayerPrefabStyle style, bool active)
        {
            if (style == PlayerPrefabStyle.MAP)
            {
                if (active)
                    onMapPlayer.SetActive(true);
                else
                    onMapPlayer.SetActive(false);
            }
            else
            {
                if (active)
                    onGamePlayer.SetActive(true);
                else
                    onGamePlayer.SetActive(false);
            }
        }
    }

    public enum PlayerPrefabStyle
    {
        MAP,
        GAME
    }
}