﻿using Devkit.Base.Component;

namespace ZombieShooter.Component.Auth
{
    public class LoginComponent : IComponent
    {
        private FirebaseComponent _firebaseComponent;

        public void Initialize(ComponentContainer componentContainer)
        {
            _firebaseComponent = componentContainer.GetComponent("FirebaseComponent") as FirebaseComponent;
        }
    }
}