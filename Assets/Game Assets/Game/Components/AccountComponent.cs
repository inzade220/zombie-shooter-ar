namespace ZombieShooter.Component
{
    using System;
    using Devkit.Base.Component;

    public class AccountComponent : IComponent
    {
        private AccountData accountData;

        private AchievementsComponent achievementsComponent;
        private GamePlayComponent gamePlayComponent;
        private AudioComponent audioComponent;
        private CurrencyComponent currencyComponent;

        public delegate void AccountComponentDelegate();

        public event AccountComponentDelegate OnLoadUserData;


        public void Initialize(ComponentContainer componentContainer)
        {
            achievementsComponent = componentContainer.GetComponent("AchievementsComponent") as AchievementsComponent;
            gamePlayComponent = componentContainer.GetComponent("GamePlayComponent") as GamePlayComponent;
            audioComponent = componentContainer.GetComponent("AudioComponent") as AudioComponent;
            currencyComponent = componentContainer.GetComponent("CurrencyComponent") as CurrencyComponent;

            SetDefaultData();
        }

        public AccountData AccountData
        {
            get => accountData;
            set => accountData = value;
        }

        private void SetDefaultData()
        {
            accountData.Username = "Default";
            accountData.PlayerLevel = 1;
            accountData.CompletedAchievements = new[] {0};
            accountData.LastReachedLevel = 1;
            accountData.MaxScore = 0;
            accountData.AudioLevel = 100;
            accountData.OwnedGold = 0;
            accountData.OwnedMoney = 0;
        }

        public string Username
        {
            get => accountData.Username;
            set => accountData.Username = value;
        }

        public bool UsernameIsDefault => Username == "Default";

        public int PlayerLevel => accountData.PlayerLevel;

        public int[] CompletedAchievements => accountData.CompletedAchievements;

        public int LastReachedLevel => accountData.LastReachedLevel;

        public int MaxScore => accountData.MaxScore;

        public int AudioLevel => accountData.AudioLevel;

        public int OwnedGold => accountData.OwnedGold;

        public int OwnedMoney => accountData.OwnedMoney;
    }

    [Serializable]
    public struct AccountData
    {
        public string Username;
        public int PlayerLevel;
        public int[] CompletedAchievements; //   Achievement Component
        public int LastReachedLevel; //  Gameplay Component
        public int MaxScore; //  Gameplay Component
        public int AudioLevel; // Audio Component
        public int OwnedGold; // Currency Component
        public int OwnedMoney; // Currency Component
    }
}