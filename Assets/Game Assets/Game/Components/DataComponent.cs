namespace ZombieShooter.Component
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Devkit.Base.Component;
    using Firebase.Database;
    using Game_Assets.Base;
    using Game_Assets.Game.Items;
    using UnityEngine;

    public class DataComponent : IComponent, ICanLoadData, ICanSaveData
    {
        private DatabaseComponent databaseComponent;
        private AccountComponent accountComponent;
        private InventoryComponent inventoryComponent;

        public delegate void DataComponentDelegate();

        public event DataComponentDelegate OnUserDataLoaded;
        public event DataComponentDelegate OnUserDataSaved;

        private List<Weapon> weapons = new List<Weapon>();
        private List<Ammo> ammos = new List<Ammo>();
        private List<Armor> armors = new List<Armor>();
        private List<Heal> heals = new List<Heal>();
        private List<Boost> boosts = new List<Boost>();
        private List<Tactical> tacticals = new List<Tactical>();

        private List<Item> allItemData = new List<Item>();

        private UserData userData;

        private bool loadingCompleted;

        public void Initialize(ComponentContainer componentContainer)
        {
            databaseComponent = componentContainer.GetComponent("DatabaseComponent") as DatabaseComponent;
            accountComponent = componentContainer.GetComponent("AccountComponent") as AccountComponent;
            inventoryComponent = componentContainer.GetComponent("InventoryComponent") as InventoryComponent;

            userData.account = AccountData;
            userData.inventory = InventoryData;

            LoadAllItems();
        }

        private void LoadAllItems()
        {
            int index = 0;
            Resources.LoadAll<Weapon>("Weapons/AR").ToList().ForEach(weapon =>
            {
                weapon.ID = index;
                weapons.Add(weapon);
                index++;
            });
            Resources.LoadAll<Weapon>("Weapons/SMG").ToList().ForEach(weapon =>
            {
                weapon.ID = index;
                weapons.Add(weapon);
                index++;
            });
            Resources.LoadAll<Weapon>("Weapons/SNIPER").ToList().ForEach(weapon =>
            {
                weapon.ID = index;
                weapons.Add(weapon);
                index++;
            });
            Resources.LoadAll<Weapon>("Weapons/PISTOL").ToList().ForEach(weapon =>
            {
                weapon.ID = index;
                weapons.Add(weapon);
                index++;
            });
            Resources.LoadAll<Weapon>("Weapons/MELEE").ToList().ForEach(weapon =>
            {
                weapon.ID = index;
                weapons.Add(weapon);
                index++;
            });
            Resources.LoadAll<Ammo>("Ammos").ToList().ForEach(ammo =>
            {
                ammo.ID = index;
                ammos.Add(ammo);
                index++;
            });
            Resources.LoadAll<Armor>("Armors/Helmets").ToList().ForEach(armor =>
            {
                armor.ID = index;
                armors.Add(armor);
                index++;
            });
            Resources.LoadAll<Armor>("Armors/Vests").ToList().ForEach(armor =>
            {
                armor.ID = index;
                armors.Add(armor);
                index++;
            });
            Resources.LoadAll<Armor>("Armors/Gloves").ToList().ForEach(armor =>
            {
                armor.ID = index;
                armors.Add(armor);
                index++;
            });
            Resources.LoadAll<Armor>("Armors/Trousers").ToList().ForEach(armor =>
            {
                armor.ID = index;
                armors.Add(armor);
                index++;
            });
            Resources.LoadAll<Armor>("Armors/Boots").ToList().ForEach(armor =>
            {
                armor.ID = index;
                armors.Add(armor);
                index++;
            });
            Resources.LoadAll<Heal>("Consumables/Heals").ToList().ForEach(heal =>
            {
                heal.ID = index;
                heals.Add(heal);
                index++;
            });
            Resources.LoadAll<Boost>("Consumables/Boosts").ToList().ForEach(boost =>
            {
                boost.ID = index;
                boosts.Add(boost);
                index++;
            });
            Resources.LoadAll<Tactical>("Tacticals").ToList().ForEach(tactical =>
            {
                tactical.ID = index;
                tacticals.Add(tactical);
                index++;
            });

            weapons.ForEach(weapon => allItemData.Add(weapon));
            ammos.ForEach(ammo => allItemData.Add(ammo));
            armors.ForEach(armor => allItemData.Add(armor));
            heals.ForEach(heal => allItemData.Add(heal));
            boosts.ForEach(boost => allItemData.Add(boost));
            tacticals.ForEach(tactical => allItemData.Add(tactical));

            Debug.Log("All items are loaded");
        }

        #region Getters

        public List<Weapon> Weapons => weapons;

        public List<Ammo> Ammos => ammos;

        public List<Armor> Armors => armors;

        public List<Heal> Heals => heals;

        public List<Boost> Boosts => boosts;

        public List<Tactical> Tacticals => tacticals;

        public AccountData AccountData
        {
            get => accountComponent.AccountData;
            set => accountComponent.AccountData = value;
        }

        public InventoryData InventoryData
        {
            get => inventoryComponent.InventoryData;
            set => inventoryComponent.InventoryData = value;
        }

        #endregion


        public struct UserData
        {
            public AccountData account;
            public InventoryData inventory;
        }

        public List<Item> AllItemData => allItemData;


        #region Loading

        public void LoadData()
        {
            Debug.Log("Start Loading");
            databaseComponent.Load(this);
        }

        public void OnLoadProcessFaulted()
        {
            throw new NotImplementedException();
        }

        public void OnLoadProcessCanceled()
        {
            throw new NotImplementedException();
        }

        public void OnLoadProcessCompleted(DataSnapshot dataSnapshot)
        {
            if (!dataSnapshot.Child("account").Exists && !dataSnapshot.Child("inventory").Exists)
                SaveDefaultUserData();
            else
            {
                AccountData = JsonUtility.FromJson<AccountData>(dataSnapshot.Child("account").ToString());
                InventoryData = JsonUtility.FromJson<InventoryData>(dataSnapshot.Child("inventory").ToString());
                Debug.Log("Data initialized");
                OnUserDataLoaded();
            }
        }

        public void OnLoadProcessDataNotExists()
        {
            SaveDefaultUserData();
        }

        private void SaveDefaultUserData()
        {
            SaveData();
        }

        #endregion

        #region Saving

        public void SaveData()
        {
            Debug.Log("Start Saving");
            userData.account = AccountData;
            userData.inventory = InventoryData;
            databaseComponent.Save(this, AccountData,"Account");
            databaseComponent.Save(this, InventoryData,"Inventory");
        }

        public void OnSaveProcessFaulted()
        {
            throw new NotImplementedException();
        }

        public void OnSaveProcessCanceled()
        {
            throw new NotImplementedException();
        }

        public void OnSaveProcessCompleted()
        {
            Debug.Log("Saved");
            //OnUserDataSaved();
        }

        #endregion
    }
}