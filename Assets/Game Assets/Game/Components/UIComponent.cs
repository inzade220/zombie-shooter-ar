namespace ZombieShooter.Component
{
    using Devkit.Base.Component;
    using ZombieShooter.UserInterface;
    using UnityEngine;

    public class UIComponent : MonoBehaviour, IComponent
    {
        public enum MenuName
        {
            SPLASH,
            LOGIN,
            REGISTER,
            MAIN_MENU,
            MAP,
            PRE_GAME,
            IN_GAME,
            SETTINGS,
            ACHIEVEMENTS,
            MARKET,
            INVENTORY,
            GARAGE,
            CO_PILOT,
            CREDITS
        }

        [SerializeField] private BaseCanvas splashCanvas = null;

        [SerializeField] private BaseCanvas loginCanvas = null;
        [SerializeField] private BaseCanvas registerCanvas = null;
        [SerializeField] private BaseCanvas mainMenuCanvas = null;
        [SerializeField] private BaseCanvas mapCanvas = null;
        [SerializeField] private BaseCanvas preGameCanvas = null;
        [SerializeField] private BaseCanvas inGameCanvas = null;
        [SerializeField] private BaseCanvas settingsCanvas = null;
        [SerializeField] private BaseCanvas achievementsCanvas = null;
        [SerializeField] private BaseCanvas marketCanvas = null;
        [SerializeField] private BaseCanvas inventoryCanvas = null;
        [SerializeField] private BaseCanvas garageCanvas = null;
        [SerializeField] private BaseCanvas coPilotCanvas = null;
        [SerializeField] private BaseCanvas creditsCanvas = null;

        private BaseCanvas activeCanvas = null;

        public void Initialize(ComponentContainer componentContainer)
        {
            splashCanvas.Initialize(componentContainer);
            loginCanvas.Initialize(componentContainer);
            registerCanvas.Initialize(componentContainer);
            mainMenuCanvas.Initialize(componentContainer);
            mapCanvas.Initialize(componentContainer);
            preGameCanvas.Initialize(componentContainer);
            inGameCanvas.Initialize(componentContainer);
            settingsCanvas.Initialize(componentContainer);
            //achievementsCanvas.Initialize(componentContainer);
            //marketCanvas.Initialize(componentContainer);
            //inventoryCanvas.Initialize(componentContainer);
            //garageCanvas.Initialize(componentContainer);
            //coPilotCanvas.Initialize(componentContainer);
            //creditsCanvas.Initialize(componentContainer);

            DeactivateCanvas(splashCanvas);
            DeactivateCanvas(loginCanvas);
            DeactivateCanvas(registerCanvas);
            DeactivateCanvas(mainMenuCanvas);
            DeactivateCanvas(mapCanvas);
            DeactivateCanvas(preGameCanvas);
            DeactivateCanvas(inGameCanvas);
            DeactivateCanvas(settingsCanvas);
            //DeactivateCanvas(achievementsCanvas);
            //DeactivateCanvas(marketCanvas);
            //DeactivateCanvas(inventoryCanvas);
            //DeactivateCanvas(garageCanvas);
            //DeactivateCanvas(coPilotCanvas);
            //DeactivateCanvas(creditsCanvas);
        }

        public BaseCanvas GetCanvas(MenuName canvas)
        {
            switch (canvas)
            {
                case MenuName.SPLASH:
                    return splashCanvas;
                case MenuName.LOGIN:
                    return loginCanvas;
                case MenuName.REGISTER:
                    return registerCanvas;
                case MenuName.MAIN_MENU:
                    return mainMenuCanvas;
                case MenuName.MAP:
                    return mapCanvas;
                case MenuName.PRE_GAME:
                    return preGameCanvas;
                case MenuName.IN_GAME:
                    return inGameCanvas;
                case MenuName.SETTINGS:
                    return settingsCanvas;
                case MenuName.ACHIEVEMENTS:
                    return achievementsCanvas;
                case MenuName.MARKET:
                    return marketCanvas;
                case MenuName.INVENTORY:
                    return inventoryCanvas;
                case MenuName.GARAGE:
                    return garageCanvas;
                case MenuName.CO_PILOT:
                    return coPilotCanvas;
                case MenuName.CREDITS:
                    return creditsCanvas;
                default:
                    return null;
            }
        }

        public void EnableCanvas(MenuName menuName)
        {
            DeactivateCanvas(activeCanvas);
            ActivateCanvas(menuName);
        }

        private void DeactivateCanvas(BaseCanvas canvas)
        {
            if (canvas)
            {
                canvas.Deactivate();
            }
        }

        private void ActivateCanvas(MenuName menuName)
        {
            switch (menuName)
            {
                case MenuName.SPLASH:
                    activeCanvas = splashCanvas;
                    break;
                case MenuName.LOGIN:
                    activeCanvas = loginCanvas;
                    break;
                case MenuName.REGISTER:
                    activeCanvas = registerCanvas;
                    break;
                case MenuName.MAIN_MENU:
                    activeCanvas = mainMenuCanvas;
                    break;
                case MenuName.MAP:
                    activeCanvas = mapCanvas;
                    break;
                case MenuName.PRE_GAME:
                    activeCanvas = preGameCanvas;
                    break;
                case MenuName.IN_GAME:
                    activeCanvas = inGameCanvas;
                    break;
                case MenuName.SETTINGS:
                    activeCanvas = settingsCanvas;
                    break;
                case MenuName.ACHIEVEMENTS:
                    activeCanvas = achievementsCanvas;
                    break;
                case MenuName.MARKET:
                    activeCanvas = marketCanvas;
                    break;
                case MenuName.INVENTORY:
                    activeCanvas = inventoryCanvas;
                    break;
                case MenuName.GARAGE:
                    activeCanvas = garageCanvas;
                    break;
                case MenuName.CO_PILOT:
                    activeCanvas = coPilotCanvas;
                    break;
                case MenuName.CREDITS:
                    activeCanvas = creditsCanvas;
                    break;
            }

            if (activeCanvas)
            {
                activeCanvas.Activate();
            }
        }
    }
}