using System.Collections.Generic;
using Devkit.Base.Component;
using Game_Assets.Game.Items;
using UnityEngine;
using ZombieShooter.Component;

namespace ZombieShooter.Component
{
    public class СhestComponent : IComponent
    {
        private DataComponent dataComponent;
        private AccountComponent accountComponent;

        private int defaultGoldAmount = 50;
        private int minGoldMultiplier = 4;
        private int maxGoldMultiplier = 10;
        private int levelMultiplier = 3;

        private int weaponDropRate = 100;
        private int ammoDropRate = 5;
        private int armorDropRate = 10;
        private int healDropRate = 50;
        private int boostDropRate = 40;
        private int tacticalDropRate = 55;

        private List<Weapon> weapons;
        private List<Ammo> ammos;
        private List<Armor> armors;
        private List<Heal> heals;
        private List<Boost> boosts;
        private List<Tactical> tacticals;

        private Dictionary<Item, int> drop;

        public void Initialize(ComponentContainer componentContainer)
        {
            dataComponent = componentContainer.GetComponent("DataComponent") as DataComponent;
            accountComponent = componentContainer.GetComponent("AccountComponent") as AccountComponent;

            if (dataComponent is { })
            {
                weapons = dataComponent.Weapons;
                ammos = dataComponent.Ammos;
                armors = dataComponent.Armors;
                heals = dataComponent.Heals;
                boosts = dataComponent.Boosts;
                tacticals = dataComponent.Tacticals;
            }

            drop = new Dictionary<Item, int>();
        }

        public Dictionary<Item, int> OpenChest()
        {
            if (weapons.Count != 0 && RandomNumber <= weaponDropRate)
            {
                var item = weapons[Random.Range(0, weapons.Count)];
                var amount = item.MaxStackAmount;
                drop.Add(item, amount);
            }

            if (ammos.Count != 0 && RandomNumber <= ammoDropRate)
            {
                var item = ammos[Random.Range(0, ammos.Count)];
                var amount = item.MaxStackAmount;
                drop.Add(item, amount);
            }

            if (armors.Count != 0 && RandomNumber <= armorDropRate)
            {
                var item = armors[Random.Range(0, armors.Count)];
                var amount = item.MaxStackAmount;
                drop.Add(item, amount);
            }

            if (heals.Count != 0 && RandomNumber <= healDropRate)
            {
                var item = heals[Random.Range(0, heals.Count)];
                var amount = item.MaxStackAmount;
                drop.Add(item, amount);
            }

            if (boosts.Count != 0 && RandomNumber <= boostDropRate)
            {
                var item = boosts[Random.Range(0, boosts.Count)];
                var amount = item.MaxStackAmount;
                drop.Add(item, amount);
            }

            if (tacticals.Count != 0 && RandomNumber <= tacticalDropRate)
            {
                var item = tacticals[Random.Range(0, tacticals.Count)];
                var amount = item.MaxStackAmount;
                drop.Add(item, amount);
            }

            return drop;
        }

        public int GetDropGold()
        {
            levelMultiplier = accountComponent.PlayerLevel;
            return defaultGoldAmount * Random.Range(minGoldMultiplier, maxGoldMultiplier) * levelMultiplier;
        }

        private int RandomNumber => Random.Range(0, 100);
    }
}