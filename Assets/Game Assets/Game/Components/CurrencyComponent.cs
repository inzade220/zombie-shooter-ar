using Devkit.Base.Component;

namespace ZombieShooter.Component
{
    public class CurrencyComponent : IComponent
    {
        private int ownedGold, ownedMoney;
        private bool isGoldEnough, isMoneyEnough;
        private DataComponent dataComponent;

        public void Initialize(ComponentContainer componentContainer)
        {
            dataComponent = componentContainer.GetComponent("DataComponent") as DataComponent;
            dataComponent.OnUserDataLoaded += SetValues;
        }

        private void SetValues()
        {
            //ownedGold = accountComponent.OwnedGold;
            //ownedMoney = accountComponent.OwnedMoney;
        }

        #region Currency Methods

        public void EarnGold(int goldIncome)
        {
            ownedGold += goldIncome;
        }

        public void EarnMoney(int moneyIncome)
        {
            ownedMoney += moneyIncome;
        }

        public bool SpendGold(int goldOutcome)
        {
            if (ownedGold < goldOutcome)
            {
                //TODO give an error
                isGoldEnough = false;
            }
            else
            {
                ownedGold -= goldOutcome;
                isGoldEnough = true;
            }

            return isGoldEnough;
        }

        public bool SpendMoney(int moneyOutcome)
        {
            if (ownedMoney < moneyOutcome)
            {
                //TODO give an error
                isMoneyEnough = false;
            }
            else
            {
                ownedMoney -= moneyOutcome;
                isMoneyEnough = true;
            }

            return isMoneyEnough;
        }

        #endregion

        public int OwnedGold => ownedGold;

        public int OwnedMoney => ownedMoney;
    }
}