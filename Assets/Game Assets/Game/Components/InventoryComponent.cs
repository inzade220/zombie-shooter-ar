﻿using Game_Assets.Game.Items;

namespace ZombieShooter.Component
{
    using System;
    using Devkit.Base.Component;
    using UnityEngine;

    public class InventoryComponent : MonoBehaviour, IComponent
    {
        private InventoryData inventoryData;
        private BackpackData backpackData;
        private EquipmentData equipmentData;

        private readonly int BACKPACK_ROW = 5;
        private readonly int BACKPACK_COLUMN = 5;
        private readonly int EQUIPMENT_SIZE = 9;
        private AccountComponent accountComponent;
        private DataComponent dataComponent;

        private InventorySlot[,] backpackSlots;
        private InventorySlot[] equipmentSlots;

        public void Initialize(ComponentContainer componentContainer)
        {
            accountComponent = componentContainer.GetComponent("AccountComponent") as AccountComponent;
            dataComponent = componentContainer.GetComponent("DataComponent") as DataComponent;
            backpackSlots = new InventorySlot[BACKPACK_ROW, BACKPACK_COLUMN];
            equipmentSlots = new InventorySlot[EQUIPMENT_SIZE];
            SetInitialData();
        }

        private void SetInitialData()
        {
            //backpackData.amounts = new[] {0};
            //backpackData.items = new[] {0};
            inventoryData.Backpack = backpackData;
            inventoryData.Equipment = equipmentData;
            for (var i = 0; i < BACKPACK_ROW; i++)
            {
                for (int j = 0; j < BACKPACK_COLUMN; j++)
                {
                    backpackSlots[i, j] = new InventorySlot(i, j);
                    backpackSlots[i, j].OnSlotItemHasMaxStackAmount += StoreItem;
                }
            }


            // 0. HELMET
            // 1. VEST
            // 2. GLOVES
            // 3. TROUSERS
            // 4. BOOTS
            // 5. PRIMARY WEAPON
            // 6. SECONDARY WEAPON
            // 7. LEFT POCKET
            // 8. RIGHT POCKET

            for (var i = 0; i < EQUIPMENT_SIZE; i++)
            {
                equipmentSlots[i] = new InventorySlot(i);
            }
        }

        #region Backpack Storing

        public void StoreItem(Item item, int amount = 0)
        {
            InventorySlot sameItemSlotInBackpack = FindItemInBackpack(item);
            InventorySlot firstEmptySlot = FindFirstEmptySlot();

            if (!BackpackIsFull())
                if (sameItemSlotInBackpack != null)
                    sameItemSlotInBackpack.Add(item);
                else
                    firstEmptySlot.Add(item);
        }

        private InventorySlot FindItemInBackpack(Item item)
        {
            for (var i = 0; i < BACKPACK_ROW; i++)
            {
                for (int j = 0; j < BACKPACK_COLUMN; j++)
                {
                    if (backpackSlots[i, j].ItemDataID == item.ID &&
                        !backpackSlots[i, j].SlotIsFull)
                        return backpackSlots[i, j];
                }
            }

            return null;
        }

        private InventorySlot FindFirstEmptySlot()
        {
            for (var i = 0; i < BACKPACK_ROW; i++)
            {
                for (int j = 0; j < BACKPACK_COLUMN; j++)
                {
                    if (backpackSlots[i, j].IsEmpty)
                        return backpackSlots[i, j];
                }
            }

            return null;
        }

        private bool BackpackIsFull()
        {
            for (var i = 0; i < BACKPACK_ROW; i++)
            {
                for (int j = 0; j < BACKPACK_COLUMN; j++)
                {
                    if (backpackSlots[i, j].IsEmpty && !backpackSlots[i, j].SlotIsFull)
                        return false;
                }
            }

            return true;
        }

        #endregion

        public void EquipItem(IEquippable equippableItem)
        {
            var itemType = equippableItem.GetType();
            Debug.Log(itemType);
        }


        public InventoryData InventoryData
        {
            get => inventoryData;
            set => inventoryData = value;
        }
    }

    public class InventorySlot
    {
        private int rowIndex;
        private int columnIndex;

        private int itemDataID;
        private int currAmount;
        private int maxAmount;

        private bool slotIsFull;

        public delegate void InventorySlotItemMaxAmountDelegate(Item item, int remainingAmount);

        public event InventorySlotItemMaxAmountDelegate OnSlotItemHasMaxStackAmount;

        public InventorySlot(int rowIndex, int columnIndex = -1)
        {
            this.rowIndex = rowIndex;
            this.columnIndex = columnIndex;
            itemDataID = -1; //NO ITEM
            currAmount = 0;
            maxAmount = 1;
        }

        public void Add(Item item, int amount = 0)
        {
            if (amount == 0)
                amount = item.Amount;
            if (itemDataID == -1)
            {
                itemDataID = item.ID;
                currAmount += amount;
                maxAmount = item.MaxStackAmount;
            }
            else if (currAmount != maxAmount)
            {
                currAmount += amount;
                if (currAmount > maxAmount)
                    OnSlotItemHasMaxStackAmount(item, maxAmount - currAmount);
            }
        }

        public int ItemDataID => itemDataID;
        public int ItemAmount => currAmount;

        public int RowIndex => rowIndex;

        public int ColumnIndex => columnIndex;

        public bool IsEmpty => itemDataID == -1;

        public bool SlotIsFull => currAmount == maxAmount;
    }

    [Serializable]
    public struct InventoryData
    {
        public BackpackData Backpack;
        public EquipmentData Equipment;
    }
    [Serializable]
    public struct BackpackData
    {
        public int Helmet;
        public int[] items;
        public int[] amounts;
    }
    [Serializable]
    public struct EquipmentData
    {
        public int Helmet;
        public int Vest;
        public int Gloves;
        public int Trousers;
        public int Boats;
        public int PrimaryWeapon;
        public int SecondaryWeapon;
        public Pocket LeftPocket;
        public Pocket RightPocket;
    }
    [Serializable]
    public struct Pocket
    {
        public int item;
        public int amount;
    }
}