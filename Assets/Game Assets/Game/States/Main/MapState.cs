using UnityEngine.SceneManagement;
using ZombieShooter.Component;
using ZombieShooter.UserInterface;

namespace ZombieShooter.State
{
    using Devkit.Base.Component;
    using Devkit.HSM;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class MapState : StateMachine
    {
        private PlayerComponent playerComponent;
        private UIComponent uiComponent;
        private MapCanvas mapCanvas;


        public MapState(ComponentContainer componentContainer)
        {
            playerComponent = componentContainer.GetComponent("PlayerComponent") as PlayerComponent;
            uiComponent = componentContainer.GetComponent("UIComponent") as UIComponent;
            mapCanvas = uiComponent.GetCanvas(UIComponent.MenuName.MAP) as MapCanvas;
        }

        protected override void OnEnter()
        {
            uiComponent.EnableCanvas(UIComponent.MenuName.MAP);
            mapCanvas.OnSettingsRequest += OnSettingsRequest;
            mapCanvas.OnReturnToMainMenu += OnReturnToMainMenu;
            mapCanvas.OnInGameRequest += OnInGameRequest;

            SceneManager.LoadSceneAsync("MapScene", LoadSceneMode.Additive).completed += operation =>
            {
                if (operation.isDone)
                    playerComponent.SwitchPlayerPrefab(PlayerPrefabStyle.MAP, true);
            };
        }

        protected override void OnExit()
        {
            SceneManager.UnloadSceneAsync("MapScene").completed -= operation =>
            {
                if (operation.isDone)
                    playerComponent.SwitchPlayerPrefab(PlayerPrefabStyle.MAP, false);
            };
        }

        protected override void OnUpdate()
        {
        }

        private void OnSettingsRequest()
        {
            SendTrigger((int) StateTriggers.GO_TO_SETTINGS_REQUEST);
        }

        private void OnReturnToMainMenu()
        {
            SendTrigger((int) StateTriggers.GO_TO_MAIN_MENU_REQUEST);
        }

        private void OnInGameRequest()
        {
            SendTrigger((int) StateTriggers.START_GAME_REQUEST);
        }
    }
}