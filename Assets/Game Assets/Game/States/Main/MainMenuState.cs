using ZombieShooter.Component;
using ZombieShooter.UserInterface;

namespace ZombieShooter.State
{
    using Devkit.Base.Component;
    using Devkit.HSM;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class MainMenuState : StateMachine
    {
        private UIComponent uiComponent;
        private MainMenuCanvas mainMenuCanvas;

        public MainMenuState(ComponentContainer componentContainer)
        {
            uiComponent = componentContainer.GetComponent("UIComponent") as UIComponent;
            mainMenuCanvas = uiComponent.GetCanvas(UIComponent.MenuName.MAIN_MENU) as MainMenuCanvas;
        }

        protected override void OnEnter()
        {
            Debug.Log("MainMenuState OnEnter");
            uiComponent.EnableCanvas(UIComponent.MenuName.MAIN_MENU);
            mainMenuCanvas.OnMapMenuRequest += OnMapMenuRequest;
            mainMenuCanvas.OnSettingsMenuRequest += OnSettingsMenuRequest;
            mainMenuCanvas.OnInventoryMenuRequest += OnInventoryMenuRequest;
            mainMenuCanvas.OnAchievementsMenuRequest += OnAchievementsMenuRequest;
        }

        protected override void OnExit()
        {
            Debug.Log("MainMenuState OnExit");
            mainMenuCanvas.OnMapMenuRequest -= OnMapMenuRequest;
            mainMenuCanvas.OnSettingsMenuRequest -= OnSettingsMenuRequest;
            mainMenuCanvas.OnInventoryMenuRequest -= OnInventoryMenuRequest;
            mainMenuCanvas.OnAchievementsMenuRequest -= OnAchievementsMenuRequest;
        }

        protected override void OnUpdate()
        {
            Debug.Log("MainMenuState OnUpdate");
        }

        private void OnMapMenuRequest()
        {
            SendTrigger((int) StateTriggers.GO_TO_MAP_REQUEST);
        }

        private void OnSettingsMenuRequest()
        {
            SendTrigger((int) StateTriggers.GO_TO_SETTINGS_REQUEST);
        }

        private void OnInventoryMenuRequest()
        {
            SendTrigger((int) StateTriggers.GO_TO_INVENTORY_REQUEST);
        }

        private void OnAchievementsMenuRequest()
        {
            SendTrigger((int) StateTriggers.GO_TO_ACHIEVEMENTS_REQUEST);
        }
    }
}