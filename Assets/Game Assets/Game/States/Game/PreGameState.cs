using GoogleARCore;
using GoogleARCoreInternal;
using UnityEngine;
using ZombieShooter.Component;

namespace ZombieShooter.State
{
    using Devkit.Base.Component;
    using Devkit.HSM;

    public class PreGameState : StateMachine
    {
        private UIComponent uiComponent;

        public PreGameState(ComponentContainer componentContainer)
        {
            uiComponent = componentContainer.GetComponent("UIComponent") as UIComponent;
        }

        protected override void OnEnter()
        {
            Debug.Log("PreGameState OnEnter");
            uiComponent.EnableCanvas(UIComponent.MenuName.PRE_GAME);
        }

        protected override void OnExit()
        {
            Debug.Log("PreGameState OnExit");
        }

        protected override void OnUpdate()
        {
            if (LifecycleManager.Instance.SessionStatus == SessionStatus.Tracking)
                SendTrigger((int) StateTriggers.PLAY_GAME_REQUEST);
        }
    }
}