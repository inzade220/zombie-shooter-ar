using UnityEngine.SceneManagement;

namespace ZombieShooter.State
{
    using Devkit.Base.Component;
    using Devkit.HSM;
    using Component;
    using UnityEngine;

    public class GameState : StateMachine
    {
        private PlayerComponent playerComponent;


        private InGameState inGameState;
        private PreGameState preGameState;
        private PauseGameState pauseGameState;
        private EndGameState endGameState;

        public GameState(ComponentContainer componentContainer)
        {
            playerComponent = componentContainer.GetComponent("PlayerComponent") as PlayerComponent;


            preGameState = new PreGameState(componentContainer);
            inGameState = new InGameState(componentContainer);
            pauseGameState = new PauseGameState(componentContainer);
            endGameState = new EndGameState(componentContainer);

            AddSubState(preGameState);
            AddSubState(inGameState);
            AddSubState(pauseGameState);
            AddSubState(endGameState);

            AddTransition(preGameState, inGameState, (int) StateTriggers.PLAY_GAME_REQUEST);
            AddTransition(inGameState, pauseGameState, (int) StateTriggers.PAUSE_GAME_REQUEST);
            AddTransition(pauseGameState, inGameState, (int) StateTriggers.RESUME_GAME_REQUEST);
            AddTransition(inGameState, endGameState, (int) StateTriggers.GAME_OVER);
            AddTransition(inGameState, preGameState, (int) StateTriggers.RESTART_GAME_REQUEST);
            AddTransition(endGameState, preGameState, (int) StateTriggers.REPLAY_GAME_REQUEST);
        }

        protected override void OnEnter()
        {
            Debug.Log("GameState OnEnter");
            playerComponent.SwitchPlayerPrefab(PlayerPrefabStyle.GAME,true);
            SceneManager.LoadSceneAsync("GameScene", LoadSceneMode.Additive);
        }

        protected override void OnExit()
        {
            Debug.Log("GameState OnExit");
            playerComponent.SwitchPlayerPrefab(PlayerPrefabStyle.GAME,false);
            SceneManager.UnloadSceneAsync("GameScene");
        }

        protected override void OnUpdate()
        {
        }
    }
}