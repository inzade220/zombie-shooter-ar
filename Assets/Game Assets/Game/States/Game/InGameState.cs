using ZombieShooter.Component;
using ZombieShooter.UserInterface;

namespace ZombieShooter.State
{
    using Devkit.Base.Component;
    using Devkit.HSM;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class InGameState : StateMachine
    {
        private UIComponent uiComponent;
        private InGameCanvas inGameCanvas;

        public InGameState(ComponentContainer componentContainer)
        {
            uiComponent = componentContainer.GetComponent("UIComponent") as UIComponent;
            inGameCanvas = uiComponent.GetCanvas(UIComponent.MenuName.IN_GAME) as InGameCanvas;
        }

        private void OnPauseButtonClick()
        {
            SendTrigger((int) StateTriggers.PAUSE_GAME_REQUEST);
        }

        private void OnRestartButtonClick()
        {
            SendTrigger((int) StateTriggers.RESTART_GAME_REQUEST);
        }

        private void OnResumeGameButtonClick()
        {
            SendTrigger((int) StateTriggers.RESUME_GAME_REQUEST);
        }

        protected override void OnEnter()
        {
            Debug.Log("InGameState OnEnter");
            uiComponent.EnableCanvas(UIComponent.MenuName.IN_GAME);

            inGameCanvas.OnPauseButtonClick += OnPauseButtonClick;
            inGameCanvas.OnRestartButtonClick += OnRestartButtonClick;
            inGameCanvas.OnResumeGameButtonClick += OnResumeGameButtonClick;
        }

        protected override void OnExit()
        {
            Debug.Log("InGameState OnExit");
        }

        protected override void OnUpdate()
        {
        }
    }
}