using ZombieShooter.Component;

namespace ZombieShooter.State
{
    using Devkit.Base.Component;
    using Devkit.HSM;
    using ZombieShooter.Component;
    using UnityEngine;

    public class AutoLoginCheckState : StateMachine
    {
        private FirebaseComponent firebaseComponent;

        public AutoLoginCheckState(ComponentContainer componentContainer)
        {
            firebaseComponent = componentContainer.GetComponent("FirebaseComponent") as FirebaseComponent;
        }

        private void OnAutoLoginCheck(bool success)
        {
            if (success)
            {
                SendTrigger((int) StateTriggers.AUTO_LOGIN_COMPLETED);
            }
            else
            {
                SendTrigger((int) StateTriggers.AUTO_LOGIN_FAILED);
            }
        }

        protected override void OnEnter()
        {
            Debug.Log("AutoLoginCheck state On Enter");
            firebaseComponent.OnAutoLoginCheck += success => { OnAutoLoginCheck(success); };
            firebaseComponent.CheckAutoLogin();
        }

        protected override void OnExit()
        {
            Debug.Log("AutoLoginCheck state On Exit");
            firebaseComponent.OnAutoLoginCheck -= success => { OnAutoLoginCheck(success); };
        }

        protected override void OnUpdate()
        {
            //Debug.Log("AutoLoginCheck state On Update");
        }
    }
}