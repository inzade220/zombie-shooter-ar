using ZombieShooter.Component;
using ZombieShooter.UserInterface;

namespace ZombieShooter.State
{
    using Devkit.Base.Component;
    using Devkit.HSM;

    public class UsernameCreateState : StateMachine
    {
        private UIComponent uiComponent;
        private SplashCanvas splashCanvas;
        private AccountComponent accountComponent;

        public UsernameCreateState(ComponentContainer componentContainer)
        {
            uiComponent = componentContainer.GetComponent("UIComponent") as UIComponent;
            splashCanvas = uiComponent.GetCanvas(UIComponent.MenuName.SPLASH) as SplashCanvas;
            accountComponent = componentContainer.GetComponent("AccountComponent") as AccountComponent;
        }

        protected override void OnEnter()
        {
            splashCanvas.ActivateUsernameCreateUI();
        }

        protected override void OnExit()
        {
        }

        protected override void OnUpdate()
        {
            if (!accountComponent.UsernameIsDefault)
                SendTrigger((int) StateTriggers.SPLASH_COMPLETED);
        }
    }
}