using Devkit.Base.Component;
using ZombieShooter.Component;
using ZombieShooter.UserInterface;

namespace ZombieShooter.State
{
    using Devkit.HSM;
    using UnityEngine;

    public class LoadingState : StateMachine
    {
        private DataComponent dataComponent;
        private AccountComponent accountComponent;
        private UIComponent uiComponent;
        private SplashCanvas splashCanvas;

        public LoadingState(ComponentContainer componentContainer)
        {
            dataComponent = componentContainer.GetComponent("DataComponent") as DataComponent;
            accountComponent = componentContainer.GetComponent("AccountComponent") as AccountComponent;
            uiComponent = componentContainer.GetComponent("UIComponent") as UIComponent;
            splashCanvas = uiComponent.GetCanvas(UIComponent.MenuName.SPLASH) as SplashCanvas;
        }

        private void OnLoadingCompleted()
        {
            dataComponent.SaveData();
        }

        private void OnSavingCompleted()
        {
            if (!accountComponent.UsernameIsDefault)
                SendTrigger((int) StateTriggers.SPLASH_COMPLETED);
            else
                SendTrigger((int) StateTriggers.USERNAME_CREATE_REQUEST);
        }

        protected override void OnEnter()
        {
            Debug.Log("Loading State On Enter");
            dataComponent.LoadData();
            dataComponent.OnUserDataLoaded += OnLoadingCompleted;
            dataComponent.OnUserDataSaved += OnSavingCompleted;
        }

        protected override void OnExit()
        {
            Debug.Log("Loading State On Exit");
            dataComponent.OnUserDataLoaded -= OnLoadingCompleted;
            dataComponent.OnUserDataSaved -= OnSavingCompleted;
        }

        protected override void OnUpdate()
        {
            Debug.Log("Loading Update");
        }
    }
}