using ZombieShooter.Component;

namespace ZombieShooter.State
{
    using Devkit.Base.Component;
    using Devkit.HSM;
    using UnityEngine;

    public class SplashState : StateMachine
    {
        private IntroState introState;
        private AutoLoginCheckState autoLoginCheckState;
        private LoadingState loadingState;
        private UsernameCreateState usernameCreateState;
        private UIComponent uiComponent;

        public SplashState(ComponentContainer componentContainer)
        {
            introState = new IntroState(componentContainer);
            autoLoginCheckState = new AutoLoginCheckState(componentContainer);
            loadingState = new LoadingState(componentContainer);
            usernameCreateState = new UsernameCreateState(componentContainer);

            uiComponent = componentContainer.GetComponent("UIComponent") as UIComponent;
            this.AddSubState(introState);
            this.AddSubState(autoLoginCheckState);
            this.AddSubState(loadingState);
            this.AddSubState(usernameCreateState);

            this.AddTransition(introState, autoLoginCheckState, (int) StateTriggers.AUTO_LOGIN_CHECK);
            this.AddTransition(autoLoginCheckState, loadingState, (int) StateTriggers.AUTO_LOGIN_COMPLETED);
            this.AddTransition(loadingState, usernameCreateState, (int) StateTriggers.USERNAME_CREATE_REQUEST);
        }

        protected override void OnEnter()
        {
            Debug.Log("Splash State OnEnter");
            uiComponent.EnableCanvas(UIComponent.MenuName.SPLASH);
        }

        protected override void OnExit()
        {
            Debug.Log("Splash State OnExit");
            this.CurrentSubState = introState;
        }

        protected override void OnUpdate()
        {
            //Debug.Log("Splash State OnUpdate");
        }
    }
}