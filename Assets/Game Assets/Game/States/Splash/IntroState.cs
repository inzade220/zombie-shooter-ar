using Devkit;
using GoogleARCore;
using ZombieShooter.Component;
using ZombieShooter.UserInterface;

namespace ZombieShooter.State
{
    using Devkit.Base.Component;
    using Devkit.HSM;
    using ZombieShooter.Component;
    using UnityEngine;

    public class IntroState : StateMachine
    {
        private UIComponent uiComponent;
        private SplashCanvas splashCanvas;
        private PermissionRequest permissionRequest;

        public IntroState(ComponentContainer componentContainer)
        {
            uiComponent = componentContainer.GetComponent("UIComponent") as UIComponent;
            splashCanvas = uiComponent.GetCanvas(UIComponent.MenuName.SPLASH) as SplashCanvas;
            permissionRequest = new PermissionRequest();
        }

        protected override void OnEnter()
        {
            Debug.Log("Intro state On Enter");

            splashCanvas.PlayIntroAnimation();
            if (!permissionRequest.HasAllNeedPermissions)
            {
                permissionRequest.PreInit();
                permissionRequest.Init();
            }
        }

        protected override void OnExit()
        {
            Debug.Log("Intro state On Exit");
            splashCanvas.IsIntroCompleted = false;
        }

        protected override void OnUpdate()
        {
            //Debug.Log("Intro state On Update");
            if (splashCanvas.IsIntroCompleted && permissionRequest.HasAllNeedPermissions)
            {
                SendTrigger((int) StateTriggers.AUTO_LOGIN_CHECK);
            }
        }
    }
}