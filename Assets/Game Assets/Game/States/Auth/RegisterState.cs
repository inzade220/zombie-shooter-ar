﻿using Devkit.Base.Component;
using Devkit.HSM;
using UnityEngine;
using ZombieShooter.Component;
using ZombieShooter.UserInterface.Register;

namespace ZombieShooter.State
{
    public class RegisterState : StateMachine
    {
        private UIComponent uiComponent;
        private RegisterCanvas registerCanvas;
        private FirebaseComponent firebaseComponent;

        public RegisterState(ComponentContainer componentContainer)
        {
            uiComponent = componentContainer.GetComponent("UIComponent") as UIComponent;
            registerCanvas = uiComponent.GetCanvas(UIComponent.MenuName.REGISTER) as RegisterCanvas;
            firebaseComponent = componentContainer.GetComponent("FirebaseComponent") as FirebaseComponent;
        }

        private void RequestRegister(string _email, string _password)
        {
            firebaseComponent.Register(_email, _password);
        }

        private void RequestRegisterCompleted(bool success)
        {
            if (success)
                SendTrigger((int) StateTriggers.REGISTER_COMPLETED);
            else
                Debug.Log("REGISTER FAILED");
        }

        private void RequestLoginCompleted(bool success)
        {
            if (success)
                SendTrigger((int) StateTriggers.LOGIN_COMPLETED);
            else
                Debug.Log("LOGIN FAILED");
        }

        private void RequestLoginCanvas()
        {
            SendTrigger((int) StateTriggers.GO_TO_LOGIN);
        }

        protected override void OnEnter()
        {
            uiComponent.EnableCanvas(UIComponent.MenuName.REGISTER);
            registerCanvas.OnRegisterButtonClick += RequestRegister;
            registerCanvas.OnAlreadyRegisteredButtonClick += RequestLoginCanvas;
            firebaseComponent.OnRegister += RequestRegisterCompleted;
            firebaseComponent.OnLogin += RequestLoginCompleted;
        }

        protected override void OnUpdate()
        {
        }

        protected override void OnExit()
        {
            registerCanvas.OnRegisterButtonClick -= RequestRegister;
            registerCanvas.OnAlreadyRegisteredButtonClick -= RequestLoginCanvas;
            firebaseComponent.OnRegister -= RequestRegisterCompleted;
            firebaseComponent.OnRegister -= RequestRegisterCompleted;
            firebaseComponent.OnLogin -= RequestLoginCompleted;
        }
    }
}