﻿using Devkit.Base.Component;
using Devkit.HSM;
using UnityEngine;
using ZombieShooter.Component;
using ZombieShooter.UserInterface.Login;
using ZombieShooter.UserInterface.Register;

namespace ZombieShooter.State
{
    public class LoginState : StateMachine
    {
        private UIComponent uiComponent;
        private LoginCanvas loginCanvas;
        private FirebaseComponent firebaseComponent;

        public LoginState(ComponentContainer componentContainer)
        {
            uiComponent = componentContainer.GetComponent("UIComponent") as UIComponent;
            loginCanvas = uiComponent.GetCanvas(UIComponent.MenuName.LOGIN) as LoginCanvas;
            firebaseComponent = componentContainer.GetComponent("FirebaseComponent") as FirebaseComponent;
        }

        private void RequestLogin(string _email, string _password)
        {
            Debug.Log("REQUEST LOGIN" + firebaseComponent);
            firebaseComponent.Login(_email, _password);
        }

        private void RequestLoginCompleted(bool success)
        {
            if (success)
                SendTrigger((int) StateTriggers.LOGIN_COMPLETED);
            else
                Debug.Log("LOGIN FAILED");
        }

        private void RequestRegisterCanvas()
        {
            SendTrigger((int) StateTriggers.GO_TO_REGISTER);
        }

        protected override void OnEnter()
        {
            Debug.Log("LOGIN STATE ON ENTER");
            uiComponent.EnableCanvas(UIComponent.MenuName.LOGIN);
            loginCanvas.OnLoginButtonClick += RequestLogin;
            loginCanvas.OnNotRegisteredButtonClick += RequestRegisterCanvas;
            firebaseComponent.OnLogin += RequestLoginCompleted;
        }

        protected override void OnUpdate()
        {
        }

        protected override void OnExit()
        {
            loginCanvas.OnLoginButtonClick -= RequestLogin;
            loginCanvas.OnNotRegisteredButtonClick -= RequestRegisterCanvas;
            firebaseComponent.OnLogin -= RequestLoginCompleted;
        }
    }
}