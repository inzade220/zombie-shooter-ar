﻿namespace ZombieShooter.State
{
    using Devkit.Base.Component;
    using Devkit.HSM;
    using UnityEngine;

    public class AuthState : StateMachine
    {
        private LoginState loginState;
        private RegisterState registerState;

        public AuthState(ComponentContainer componentContainer)
        {
            loginState = new LoginState(componentContainer);
            registerState = new RegisterState(componentContainer);
            this.AddSubState(loginState);
            this.AddSubState(registerState);

            this.AddTransition(loginState, registerState, (int) StateTriggers.GO_TO_REGISTER);
            this.AddTransition(registerState, loginState, (int) StateTriggers.GO_TO_LOGIN);
        }

        protected override void OnEnter()
        {
            Debug.Log("AuthState OnEnter");
        }

        protected override void OnExit()
        {
            Debug.Log("AuthState OnExit");
        }

        protected override void OnUpdate()
        {
            //Debug.Log("AuthState OnUpdate");
        }
    }
}