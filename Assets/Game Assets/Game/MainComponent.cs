namespace ZombieShooter
{
    using Devkit.Base.Component;
    using ZombieShooter.Component;
    using ZombieShooter.State;
    using System;
    using UnityEngine;

    public class MainComponent : MonoBehaviour
    {
        private ComponentContainer componentContainer;
        private AccountComponent accountComponent;
        private UIComponent uIComponent;
        private AchievementsComponent achievementsComponent;
        private AudioComponent audioComponent;
        private GamePlayComponent gamePlayComponent;
        private СhestComponent chestComponent;
        private PlayerComponent playerComponent;
        private NotificationComponent notificationComponent;
        private TutorialComponent tutorialComponent;
        private InventoryComponent inventoryComponent;
        private CurrencyComponent currencyComponent;
        private DataComponent dataComponent;
        private DatabaseComponent databaseComponent;
        private FirebaseComponent firebaseComponent;

        private AppState appState;

        private void Awake()
        {
            componentContainer = new ComponentContainer();
        }

        private void Start()
        {
            CreateAccountComponent();
            CreateUIComponent();
            CreateAchievementsComponent();
            CreateAudioComponent();
            CreateNotificationComponent();
            CreateGamePlayComponent();
            CreateChestComponent();
            CreateCurrencyComponent();
            CreatePlayerComponent();
            CreateTutorialComponent();
            CreateInventoryComponent();
            CreateDataComponent();
            CreateDatabaseComponent();
            CreateFirebaseComponent();

            InitializeComponents();

            CreateAppState();
            appState.Enter();
        }

        public void Update()
        {
            appState.Update();
        }

        private void CreateAccountComponent()
        {
            accountComponent = new AccountComponent();
            componentContainer.AddComponent("AccountComponent", accountComponent);
        }

        private void CreateUIComponent()
        {
            uIComponent = FindObjectOfType<UIComponent>();
            //TODO: check is there any ui component object in the scene!!
            componentContainer.AddComponent("UIComponent", uIComponent);
        }

        private void CreateAchievementsComponent()
        {
            achievementsComponent = new AchievementsComponent();
            componentContainer.AddComponent("AchievementsComponent", achievementsComponent);
        }

        private void CreateAudioComponent()
        {
            audioComponent = FindObjectOfType<AudioComponent>();
            componentContainer.AddComponent("AudioComponent", audioComponent);
        }

        private void CreateNotificationComponent()
        {
            notificationComponent = new NotificationComponent();
            componentContainer.AddComponent("NotificationComponent", notificationComponent);
        }

        private void CreateGamePlayComponent()
        {
            gamePlayComponent = FindObjectOfType<GamePlayComponent>();
            componentContainer.AddComponent("GamePlayComponent", gamePlayComponent);
        }

        private void CreateChestComponent()
        {
            chestComponent = new СhestComponent();
            componentContainer.AddComponent("ChestComponent", chestComponent);
        }

        private void CreatePlayerComponent()
        {
            playerComponent = FindObjectOfType<PlayerComponent>();
            componentContainer.AddComponent("PlayerComponent", playerComponent);
        }

        private void CreateTutorialComponent()
        {
            tutorialComponent = new TutorialComponent();
            componentContainer.AddComponent("TutorialComponent", tutorialComponent);
        }

        private void CreateInventoryComponent()
        {
            inventoryComponent = gameObject.AddComponent<InventoryComponent>();
            componentContainer.AddComponent("InventoryComponent", inventoryComponent);
        }

        private void CreateCurrencyComponent()
        {
            currencyComponent = new CurrencyComponent();
            componentContainer.AddComponent("CurrencyComponent", currencyComponent);
        }

        private void CreateDataComponent()
        {
            dataComponent = new DataComponent();
            componentContainer.AddComponent("DataComponent", dataComponent);
        }

        private void CreateDatabaseComponent()
        {
            databaseComponent = FindObjectOfType<DatabaseComponent>();
            componentContainer.AddComponent("DatabaseComponent", databaseComponent);
        }

        private void CreateFirebaseComponent()
        {
            firebaseComponent = FindObjectOfType<FirebaseComponent>();
            componentContainer.AddComponent("FirebaseComponent", firebaseComponent);
        }

        private void InitializeComponents()
        {
            
            uIComponent.Initialize(componentContainer);
            achievementsComponent.Initialize(componentContainer);
            audioComponent.Initialize(componentContainer);
            notificationComponent.Initialize(componentContainer);
            gamePlayComponent.Initialize(componentContainer);
            chestComponent.Initialize(componentContainer);
            currencyComponent.Initialize(componentContainer);
            playerComponent.Initialize(componentContainer);
            dataComponent.Initialize(componentContainer);
            accountComponent.Initialize(componentContainer);
            inventoryComponent.Initialize(componentContainer);
            databaseComponent.Initialize(componentContainer);
            firebaseComponent.Initialize(componentContainer);
        }

        private void CreateAppState()
        {
            appState = new AppState(componentContainer);
        }
    }
}