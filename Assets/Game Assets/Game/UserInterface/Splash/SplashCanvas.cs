using TMPro;
using UnityEngine.UI;
using ZombieShooter.Component;

namespace ZombieShooter.UserInterface
{
    using System.Collections;
    using UnityEngine;

    public class SplashCanvas : BaseCanvas
    {
        [SerializeField] private Logo publisherLogo;
        [SerializeField] private Logo appLogo;
        [SerializeField] private LoadingIcon loadingIcon;
        [SerializeField] private TMP_InputField usernameInput;
        [SerializeField] private Button saveButton;

        private AccountComponent accountComponent;

        private const float animationTime = 1.0f;

        private bool isIntroAnimCompleted;

        protected override void Init()
        {
            DeactivateUsernameCreateUI();
            accountComponent = componentContainer.GetComponent("AccountComponent") as AccountComponent;
        }

        public void ActivateUsernameCreateUI()
        {
            usernameInput.gameObject.SetActive(true);
            saveButton.gameObject.SetActive(true);
        }

        public void DeactivateUsernameCreateUI()
        {
            usernameInput.gameObject.SetActive(false);
            saveButton.gameObject.SetActive(false);
        }

        public void OnSaveUsernameButtonClick()
        {
            accountComponent.Username = usernameInput.text;
        }

        public void PlayIntroAnimation()
        {
            StartCoroutine("CoIntroAnim");
        }

        public void PlayLoadingAnimation()
        {
            loadingIcon.PlayLoadingAnimation();
        }

        public void StopLoadingAnimation()
        {
            loadingIcon.StopLoadingAnimation();
        }

        private IEnumerator CoIntroAnim()
        {
            yield return new WaitForSeconds(animationTime + 2);

            isIntroAnimCompleted = true;
        }

        public bool IsIntroCompleted
        {
            get => isIntroAnimCompleted;
            set => isIntroAnimCompleted = value;
        }
    }
}