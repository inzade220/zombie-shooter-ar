using System;
using DG.Tweening;

namespace ZombieShooter.UserInterface
{
    using System.Collections;
    using UnityEngine;

    public class LoadingIcon : MonoBehaviour
    {
        public void PlayLoadingAnimation()
        {
            gameObject.SetActive(true);
        }

        public void StopLoadingAnimation()
        {
            gameObject.SetActive(false);
        }
    }
}