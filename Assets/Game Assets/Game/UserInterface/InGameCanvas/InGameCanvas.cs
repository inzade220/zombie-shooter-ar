using UnityEngine;

namespace ZombieShooter.UserInterface
{
    public class InGameCanvas : BaseCanvas
    {
        [SerializeField] private GameObject pausePanel;

        #region Delegates

        public delegate void InGameCanvasDelegate();

        public event InGameCanvasDelegate OnPauseButtonClick;
        public event InGameCanvasDelegate OnRestartButtonClick;
        public event InGameCanvasDelegate OnResumeGameButtonClick;

        #endregion


        protected override void Init()
        {
            SwitchPausePanel(false);
        }

        public void RequestPause()
        {
            SwitchPausePanel(true);
            if (OnPauseButtonClick != null)
                OnPauseButtonClick();
        }

        public void RequestRestart()
        {
            if (OnRestartButtonClick != null)
                OnRestartButtonClick();
        }

        public void RequestResume()
        {
            if (OnResumeGameButtonClick != null)
                OnResumeGameButtonClick();
        }
        

        
        public void ClosePausePanel()
        {
            SwitchPausePanel(false);
        }

        private void SwitchPausePanel(bool on)
        {
            if (on)
                pausePanel.SetActive(true);
            else
                pausePanel.SetActive(false);
        }
    }
}