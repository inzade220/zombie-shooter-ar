namespace ZombieShooter.UserInterface 
{
    public interface ICanvasElement
    {
        void Activate();
        void Deactivate();
    }
}


