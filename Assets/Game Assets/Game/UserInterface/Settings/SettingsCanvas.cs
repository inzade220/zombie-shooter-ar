using UnityEngine;
using ZombieShooter.Component;

namespace ZombieShooter.UserInterface
{
    public class SettingsCanvas : BaseCanvas
    {
        private UIComponent uiComponent;
        private MainMenuCanvas mainMenuCanvas;

        protected override void Init()
        {
            uiComponent = componentContainer.GetComponent("UIComponent") as UIComponent;
            mainMenuCanvas = (MainMenuCanvas) uiComponent.GetCanvas(UIComponent.MenuName.MAIN_MENU);

            mainMenuCanvas.OnSettingsMenuRequest += test;
        }

        public void test()
        {
            Debug.Log("DENEME");
        }
    }
}