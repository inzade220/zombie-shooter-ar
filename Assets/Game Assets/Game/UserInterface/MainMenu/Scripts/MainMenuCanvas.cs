namespace ZombieShooter.UserInterface 
{
    public class MainMenuCanvas : BaseCanvas
    {
        public delegate void MenuRequestDelegate();

        public event MenuRequestDelegate OnMapMenuRequest;
        public event MenuRequestDelegate OnSettingsMenuRequest;
        public event MenuRequestDelegate OnAchievementsMenuRequest;
        public event MenuRequestDelegate OnInventoryMenuRequest;

        protected override void Init()
        {
            
        }
        public void RequestMapMenu() 
        {
            if (OnMapMenuRequest != null) 
            {
                OnMapMenuRequest();
            }
        }
        public void RequestSettingsMenu() 
        {
            if (OnSettingsMenuRequest != null) 
            {
                OnSettingsMenuRequest();
            }
        }

        public void RequestAchievementsMenu()
        {
            if (OnAchievementsMenuRequest != null)
            {
                OnAchievementsMenuRequest();
            }
        }

        public void RequestInventoryMenu()
        {
            if (OnInventoryMenuRequest != null)
            {
                OnInventoryMenuRequest();
            }
        }

    }
}


