namespace ZombieShooter.UserInterface
{
    public class MapCanvas : BaseCanvas
    {
        public delegate void MapCanvasDelegate();

        public event MapCanvasDelegate OnSettingsRequest;
        public event MapCanvasDelegate OnInGameRequest;
        public event MapCanvasDelegate OnReturnToMainMenuRequest;

        protected override void Init()
        {
        }

        public void RequestSettings()
        {
            if (OnSettingsRequest != null)
                OnSettingsRequest();
        }

        public void RequestInGame()
        {
            if (OnInGameRequest != null)
                OnInGameRequest();
        }

        public void RequestReturnToMainMenu()
        {
            if (OnReturnToMainMenuRequest != null)
                OnReturnToMainMenuRequest();
        }
    }
}