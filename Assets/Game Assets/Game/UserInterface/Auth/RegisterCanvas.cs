﻿using TMPro;
using UnityEngine;

namespace ZombieShooter.UserInterface.Register
{
    public class RegisterCanvas : BaseCanvas
    {
        [Header("INPUTS")] [SerializeField] private TMP_InputField email;
        [SerializeField] private TMP_InputField password;
        [SerializeField] private TMP_InputField repeatPassword;

        public delegate void RegisterCanvasRegisterButtonDelegate(string _email, string _password);

        public delegate void RegisterCanvasLoginTransitionDelegate();

        public event RegisterCanvasRegisterButtonDelegate OnRegisterButtonClick;
        public event RegisterCanvasLoginTransitionDelegate OnAlreadyRegisteredButtonClick;

        public void ClickRegisterButton()
        {
            if (OnRegisterButtonClick != null)
            {
                if (password.text == repeatPassword.text)
                    OnRegisterButtonClick(email.text, password.text);
            }
        }

        public void ClickAlreadyRegisteredButton()
        {
            if (OnAlreadyRegisteredButtonClick != null)
                OnAlreadyRegisteredButtonClick();
        }
    }
}