﻿using TMPro;
using UnityEngine;

namespace ZombieShooter.UserInterface.Login
{
    public class LoginCanvas : BaseCanvas
    {
        [Header("INPUTS")] [SerializeField] private TMP_InputField email;
        [SerializeField] private TMP_InputField password;

        public delegate void LoginCanvasLoginButtonDelegate(string _email, string _password);

        public delegate void LoginCanvasRegisterTransitionDelegate();

        public event LoginCanvasLoginButtonDelegate OnLoginButtonClick;
        public event LoginCanvasRegisterTransitionDelegate OnNotRegisteredButtonClick;

        public void ClickLoginButton()
        {
            if (OnLoginButtonClick != null)
            {
                if (email.text != "" && password.text != "")
                    OnLoginButtonClick(email.text, password.text);
                else
                    Debug.LogWarning($"Wrong Input Error!");
            }
        }

        public void ClickNotRegisteredButtonClick()
        {
            if (OnNotRegisteredButtonClick != null)
            {
                OnNotRegisteredButtonClick();
            }
        }
    }
}