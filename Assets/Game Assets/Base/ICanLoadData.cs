﻿using Firebase.Database;

namespace Game_Assets.Base
{
    public interface ICanLoadData
    {
        void LoadData();
        void OnLoadProcessFaulted();
        void OnLoadProcessCanceled();
        void OnLoadProcessCompleted(DataSnapshot dataSnapshot);
        void OnLoadProcessDataNotExists();
    }
}