﻿using Firebase.Database;

namespace Game_Assets.Base
{
    public interface ICanSaveData
    {
        void SaveData();
        void OnSaveProcessFaulted();
        void OnSaveProcessCanceled();
        void OnSaveProcessCompleted();
    }
}