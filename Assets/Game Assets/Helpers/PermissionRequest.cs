﻿using System;
using System.Collections.Generic;
using Devkit.Base.Object;
using GoogleARCore;
using UnityEngine;
using UnityEngine.Android;

namespace Devkit
{
    public class PermissionRequest : IInitializable, IDisposable
    {
        private List<string> permissionsToRequest = new List<string>();
        private PermissionCallbacks callbacks;
        private bool canRequestNewPermission = true;

        public void RequestPermission(string foo)
        {
            if (canRequestNewPermission)
            {
                Permission.RequestUserPermission(permissionsToRequest[0], callbacks);
                canRequestNewPermission = false;
            }
        }

        internal void PermissionCallbacks_PermissionDeniedAndDontAskAgain(string permissionName)
        {
            Debug.Log($"{permissionName} PermissionDeniedAndDontAskAgain");
        }

        internal void PermissionCallbacks_PermissionGranted(string permissionName)
        {
            Debug.Log($"{permissionName} PermissionCallbacks_PermissionGranted");
            permissionsToRequest.Remove(permissionsToRequest[0]);
            canRequestNewPermission = true;
        }

        internal void PermissionCallbacks_PermissionDenied(string permissionName)
        {
            Debug.Log($"{permissionName} PermissionCallbacks_PermissionDenied");
        }

        public void PreInit()
        {
            permissionsToRequest.Add(Permission.Camera);
            permissionsToRequest.Add(Permission.FineLocation);

            callbacks = new PermissionCallbacks();
            callbacks.PermissionDenied += PermissionCallbacks_PermissionDenied;
            callbacks.PermissionGranted += PermissionCallbacks_PermissionGranted;
            callbacks.PermissionGranted += RequestPermission;
            callbacks.PermissionDeniedAndDontAskAgain += PermissionCallbacks_PermissionDeniedAndDontAskAgain;
        }

        public void Init()
        {
            RequestPermission(permissionsToRequest[0]);
        }

        public bool CanRequestNewPermission => canRequestNewPermission;

        public bool HasAllNeedPermissions =>
            Permission.HasUserAuthorizedPermission(Permission.Camera)
            &&
            Permission.HasUserAuthorizedPermission(Permission.FineLocation);

        public void Dispose()
        {
            callbacks.PermissionDenied -= PermissionCallbacks_PermissionDenied;
            callbacks.PermissionGranted -= PermissionCallbacks_PermissionGranted;
            callbacks.PermissionDeniedAndDontAskAgain -= PermissionCallbacks_PermissionDeniedAndDontAskAgain;
        }
    }
}